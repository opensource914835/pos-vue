import { createApp, markRaw } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import PrimeVue from 'primevue/config';

import '@/assets/styles.scss';
import './axios';
import { useAuthStore } from './stores/auth';


( async () => {
const app = createApp(App)
const pinia = createPinia();

pinia.use(({ store }) => { store.router = markRaw(router) });

app.use(pinia);

const {getUser} = useAuthStore();
await getUser();

app.use(router)
app.use(PrimeVue, { ripple: true });

app.mount('#app')
})();