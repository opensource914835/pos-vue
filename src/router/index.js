import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AppLayout from '@/layout/AppLayout.vue'
import { useAuthStore } from '../stores/auth'

const routes = [
    {
        path: '/',
        component: AppLayout,
        children: [
            {
                path: '/',
                component: HomeView,
                meta: {
                    title: 'Dashboard',
                    isAuth: true,
                    isAdmin: false
                }
            },
            {
                path: '/category',
                component: () => import('../views/master/CategoryView.vue'),
                meta: {
                    title: 'Category',
                    isAuth: true,
                    isAdmin: false
                }
            },
            {
                path: '/satuan',
                component: () => import('../views/master/SatuanView.vue'),
                meta: {
                    title: 'Satuan',
                    isAuth: true,
                    isAdmin: true
                }
            },
            {
                path: '/barang',
                component: () => import('../views/master/BarangView.vue'),
                meta: {
                    title: 'Barang',
                    isAuth: true,
                    isAdmin: false
                }
            }
        ]
    },
    {
        path: '/login',
        name: 'auth.login',
        component: () => import('../views/auth/LoginView.vue'),
        meta: { 
            isAuth: false,
            title: 'Login'
        }
    },
    {
        path: '/register',
        name: 'auth.register',
        component: () => import('../views/auth/RegisterView.vue'),
        meta: { 
            isAuth: false,
            title: 'Register'
        }
    }
  ]
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
});

router.beforeEach( async (to, from) => {
    const useAuth = useAuthStore();
    if(to.meta.isAuth)
    {
        if(useAuth.getAuthUser)
        {
            if(to.meta.isAdmin)
            {
                if(useAuth.getRoleUser)
                {
                    return true
                }else{
                    return {
                        path: '/'
                    }
                }
            }
        }else{
            return {
                name: 'auth.login'
            }
        }
    }
    if(! to.meta.isAuth)
    {
        if(! useAuth.getAuthUser)
        {
            return true
        }else{
            return {
                path: '/'
            }
        }
    }
    
    document.title = to.meta?.title ?? "Default Title";
})

export default router
