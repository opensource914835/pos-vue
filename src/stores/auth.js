import axios from "axios";
import { defineStore } from "pinia";

export const useAuthStore = defineStore('auth', {
    state: () => ({
        isAuth: false,
        authErrors: {},
        isAdmin: false,
        isLoading: false
    }),
    getters: {
        getAuthUser: (state) => state.isAuth,
        getAuthErrors: (state) => state.authErrors,
        getRoleUser: (state) => state.isAdmin,
        getIsLoading: (state) => state.isLoading
    },
    actions: {
        async getCsrf(){
            try {
                await axios.get('/sanctum/csrf-cookie');
            } catch (error) {
                console.error(error);
            }
        },

        async handleRegisterUser(dataRegister){
            try {
                this.isLoading = true;
                const data = {
                    name: dataRegister.name,
                    email: dataRegister.email,
                    password: dataRegister.password,
                    password_confirmation: dataRegister.password_confirmation
                }
                await this.getCsrf();
                const response = await axios.post('/api/register', data);
                if(response.status === 201)
                {
                    this.getUser();
                }
            } catch (error) {
                this.authErrors = error.response.data.errors;
                this.isAuth = false;
            } finally {
                this.isLoading = false;
            }
        },
        
        async handleLoginUser(dataLogin){
            try {
                this.isLoading = true;
                const data = {
                    email: dataLogin.email,
                    password: dataLogin.password
                }
                await this.getCsrf()
                const response = await axios.post('/api/login', data);
                if (response.status === 200) {
                    this.isAuth = true;
                    this.authErrors = {};
                    this.isAdmin = (response.data.is_admin === "admin") ? true : false;
                    this.router.push({path: '/'});
                }
            } catch (error) {
                this.isAuth = false;
                if(error.response.status === 422)
                {
                    this.authErrors = error.response.data.errors;
                }
                if(error.response.status === 429)
                {
                    this.authErrors = error.response.data.errors
                }
            } finally {
                this.isLoading = false;
            }
        },

        async handleLogoutUser(){
            try {
                await axios.post('/api/logout');
                this.$reset();
                this.router.push({path: '/login'});
            } catch (error) {
                console.error(error);
            }
        },

        async getUser(){
            try {
                let user = await axios.get('/api/user');
                this.$reset;
                this.isAuth = true;
                this.isAdmin = (user.data.is_admin === 1) ? true : false;
            } catch (error) {
                this.$reset();
                this.router.push({path: '/login'})
            }
        }
    }
})